#!/bin/bash

set -e

# A script that when run as
# `./onchangedo.sh <file> <command>`
# executes the `<command>` every time the `<file>` changes
# @DEPENDS notify-tools

# dependency check
if hash inotifywait 2>/dev/null; then
  if (( $# == 2 )); then
    FILE="$1"
    COMMAND="$2"
    if [ -f "$FILE" ]; then
      while inotifywait -e close_write "$FILE" || true; do eval "$COMMAND"; done
    fi
  else
    echo "Usage: ./onchangedo.sh <file> <command>"
  fi
else
  echo "This script requires notify-tools to be installed!"
  echo "(Specifically the 'inotifywait' command)"
  exit 1
fi

