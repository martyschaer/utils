#!/bin/bash
# Picks a random file from a given directory
set -e

if [ $# -eq 1 ] || [ $# != 2 ]; then
  echo "Usage: random.sh [<filter>] <directory>"
  echo "<filter>: PCRE-regex for filtering filenames"
  echo "<directory>: directory to pick files from"
  echo "If the directory is empty, or the filter matched nothing, nothing is returned"
  exit 1
fi

if [ $# -eq 2 ]; then
  dir=${2%/}
  filter=$1
else
  dir=${1%/}
  filter=".*"
fi

if [ ! -d $dir ]; then
  echo "Script requires a directory."
  exit 1
fi

echo "$dir/$(ls "$dir" | grep -P "$filter" | sort -R | tail -n 1)"
